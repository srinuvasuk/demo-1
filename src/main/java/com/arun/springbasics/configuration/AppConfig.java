package com.arun.springbasics.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.arun.springbasics.javaconfig.Person;

@Configuration
@ComponentScan("com.arun.springbasics")
public class AppConfig {

	@Bean
	public Person getPersonObject() {
		return new Person();
	}
}
